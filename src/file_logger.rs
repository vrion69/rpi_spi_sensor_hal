
pub mod file_logger
{
    use std::fs;
    use std::fs::File;
    use std::io::Write;
    use std::path::Path;
    use dlt_wrapper_rs::{DltLogger, DltLogLevelType, dlt_println, dlt_info};

    pub struct FileLogger<'a> {
        path: &'a Path,
        current_file: Option<File>,
        logger: DltLogger,
        file_name: String,
    }

    impl FileLogger<'_>
    {
        pub fn new(file_path: &str) -> FileLogger {
            let mut logger = DltLogger::new("SHAL", "FLOG");
            let path = Path::new(file_path);
            if !path.exists() {
                fs::create_dir_all(path).unwrap_or_else(|err| {
                    dlt_info!(logger, "Failed to create dir err {}\n", err);
                    panic!("Failed to create dir");
                });
            }
            FileLogger {
                path: path,
                current_file: None,
                logger: logger,
                file_name: "temp.log".to_string(),
            }
        }

        pub fn set_file_name(&mut self, file_name: &str) {
            self.file_name = file_name.to_string();
        }

        pub fn start_logging(&mut self){
            if self.current_file.is_some() {
                dlt_info!(self.logger, "Already logging\n");
            } else {
                let file = fs::OpenOptions::new()
                    .create(true)
                    .append(true)
                    .open(self.path.join(&self.file_name));
                match file {
                    Ok(file) => {
                        dlt_info!(self.logger, "Logging started\n");
                        self.current_file = Some(file);
                    },
                    Err(err) => {
                        dlt_info!(self.logger, "Failed to create file err {}\n", err);
                    }
                }
            } 
        }

        pub fn stop_logging(&mut self) {
            if let Some(file) = &mut self.current_file {
                file.sync_all().unwrap();
                self.current_file = None;
                dlt_info!(self.logger, "Logging stopped\n");
            } else {
                //dlt_info!(self.logger, "Is not logging\n");
            }
            self.current_file = None;
        }

        pub fn log(&mut self, message: &str) {
            if let Some(file) = &mut self.current_file {
                file.write_all(message.as_bytes()).unwrap();
            }
        }
    }
}