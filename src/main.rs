pub mod mpu6500;
pub mod file_logger;

use dlt_wrapper_rs::{DltLogger, DltLogLevelType, dlt_println, dlt_info, dlt_err};
use crate::mpu6500::mpu6500::{Mpu6500Drv, SensorData};
use crate::file_logger::file_logger::FileLogger;
use rppal::i2c::I2c;
use rppal::gpio::Gpio;
use std::sync::mpsc;

const INT_PIN: u8 = 12;
const BTN_PIN: u8 = 26;

enum IpcEvents{
    Button(rppal::gpio::Level),
    SensorData(SensorData),
}




fn main() {
    let mut logger: DltLogger = DltLogger::new("SHAL", "Main");
    let (tx, rx) = mpsc::channel::<IpcEvents>();
    dlt_info!(logger, "Sensor hal started\n");
    let i2c_bus = I2c::new().unwrap_or_else(|err| {
        dlt_err!(logger, "Failed to create i2c bus err {}\n", err);
        panic!("Failed to create i2c bus");
    });

    let mut mpu6500_drv: Mpu6500Drv = Mpu6500Drv::new(i2c_bus);
    let gpio = Gpio::new().unwrap_or_else(|err| {
        dlt_err!(logger, "Failed to create gpio err {}\n", err);
        panic!("Failed to create gpio");
    });
    let int_pin = gpio.get(INT_PIN).unwrap_or_else(|err| {
        dlt_err!(logger, "Failed to irq pin err {}\n", err);
        panic!("Failed to get IRQ pin");
    });
    let mut int_pin = int_pin.into_input_pullup();

    let btn = gpio.get(BTN_PIN).unwrap_or_else(|err| {
        dlt_info!(logger, "Failed to get btn pin err {}\n", err);
        panic!("Failed to get btn pin");
    });
    let mut btn = btn.into_input_pullup();
    let mut file_logger = FileLogger::new("/home/root/shal/");

    mpu6500_drv.init();

    int_pin.set_interrupt(rppal::gpio::Trigger::FallingEdge).unwrap_or_else(|err| {
        dlt_err!(logger, "Failed to set IRQ interrupt err {}\n", err);
        panic!("Failed to set interrupt");
    });
    btn.set_interrupt(rppal::gpio::Trigger::Both).unwrap_or_else(|err| {
        dlt_err!(logger, "Failed to set BTN interrupt err {}\n", err);
        panic!("Failed to set interrupt");
    });
    let mut is_logging_started = false;
    let tx1 = tx.clone();
    let _ = int_pin.set_async_interrupt(rppal::gpio::Trigger::FallingEdge, move |_| {
        let mut logger = DltLogger::new("SHAL", "MIRQ");
        let mut sens_data : SensorData = SensorData {
                accel: [0; 3],
                gyro: [0; 3],
                quat: [0; 4],
                timestamp: 0
            };
        let (res, _) = mpu6500_drv.read_sensor_data(&mut sens_data);
        if 0 != res {
            dlt_err!(logger, "Failed to read sensor data res {}\n", res);
        }
        tx1.send(IpcEvents::SensorData(sens_data)).unwrap_or_else(|err| {
            dlt_err!(logger, "Failed to send sensor data err {}\n", err);
            panic!("Failed to send sensor data");
        });
    });
    
    let mut btn_state = rppal::gpio::Level::High;
    let _ = btn.set_async_interrupt(rppal::gpio::Trigger::Both, move |lvl| {
        let mut logger = DltLogger::new("SHAL", "BTN");
        if btn_state != lvl {
            btn_state = lvl;
            tx.send(IpcEvents::Button(lvl)).unwrap_or_else(|err| {
                dlt_err!(logger, "Failed to send btn state err {}\n", err);
                panic!("Failed to send btn state")
            });
        }
    });


    let start = std::time::Instant::now();
    loop{

        let event = rx.recv().unwrap_or_else(|err| {
            dlt_info!(logger, "Failed to recv event err {}\n", err);
            panic!("Failed to recv event");
        });

        match event {
            IpcEvents::Button(lvl) => {
                dlt_info!(logger, "Button state {:?}\n", lvl);
                if rppal::gpio::Level::Low == lvl {
                    file_logger.start_logging();
                    is_logging_started = true;
                } else {
                    file_logger.stop_logging();
                    is_logging_started = false;
                }
            },
            IpcEvents::SensorData(sens_data) => {
                if is_logging_started {
                    let log_data = format!("{} {} {} {} {} {} {} {} {} {} {}\n", sens_data.timestamp,
                    sens_data.accel[0], sens_data.accel[1], sens_data.accel[2], 
                    sens_data.gyro[0], sens_data.gyro[1], sens_data.gyro[2], 
                    sens_data.quat[0], sens_data.quat[1], sens_data.quat[2], sens_data.quat[3]);
                    dlt_info!(logger, "Log data {}\n", log_data);
                    file_logger.log(&log_data);
                }
            }
        }
        let elapsed = start.elapsed();
        if elapsed.as_secs() > 1000 {
            file_logger.stop_logging();
            break;
        }
    }
}
