#![allow(warnings)]

pub mod mpu6500 {
    use core::ffi::CStr;
    use core::slice;
    use rppal::i2c::I2c;
    use dlt_wrapper_rs::{DltLogger, DltLogLevelType, dlt_println, dlt_err, dlt_info};
    use std::time::{SystemTime};

    const INV_X_GYRO: u8 = 0x40;
    const INV_Y_GYRO: u8 = 0x20;
    const INV_Z_GYRO: u8 = 0x10;
    const INV_XYZ_GYRO: u8 = INV_X_GYRO | INV_Y_GYRO | INV_Z_GYRO;
    const INV_XYZ_ACCEL: u8 = 0x08;
    const INV_WXYZ_QUAT: i16 = 0x100;
    const MPU_200_HZ: u16 = 200;
    const GYRO_ORIENTATION: [i8; 9] = [-1, 0, 0, 0, -1, 0, 0, 0, 1];

    const DMP_FEATURE_TAP: u16 = 0x001;
    const DMP_FEATURE_ANDROID_ORIENT: u16 = 0x002;
    const DMP_FEATURE_LP_QUAT: u16 = 0x004;
    const DMP_FEATURE_PEDOMETER: u16 = 0x008;
    const DMP_FEATURE_6X_LP_QUAT: u16 = 0x010;
    const DMP_FEATURE_GYRO_CAL: u16 = 0x020;
    const DMP_FEATURE_SEND_RAW_ACCEL: u16 = 0x040;
    const DMP_FEATURE_SEND_RAW_GYRO: u16 = 0x080;
    const DMP_FEATURE_SEND_CAL_GYRO: u16 = 0x100;

    #[repr(C)]
    #[derive(Debug)]
    pub struct SensorData {
        pub timestamp: u32,
        pub accel: [i16; 3],
        pub gyro: [i16; 3],
        pub quat: [i32; 4],
    }

    extern "C" {
        fn mpu_init() -> i32;
        fn mpu_set_sensors(sensors: u8) -> i32;
        fn mpu_configure_fifo(sensors: u8) -> i32;
        fn mpu_set_sample_rate(hz: u16) -> i32;
        fn mpu_get_sample_rate(rate: *mut u16) -> i32;
        fn mpu_get_gyro_fsr(fsr: *mut u16) -> i32;
        fn mpu_get_accel_fsr(fsr: *mut u16) -> i32;
        fn dmp_load_motion_driver_firmware() -> i32;
        fn dmp_set_orientation(orient: u16) -> i32;
        fn dmp_enable_feature(mask: u16) -> i32;
        fn dmp_set_fifo_rate(rate: u16) -> i32;
        fn mpu_set_dmp_state(enable: u8) -> i32;
        fn dmp_read_fifo(
            gyro: *mut i16,
            accel: *mut i16,
            quat: *mut i32,
            timestamp: *mut u32,
            sensors: *mut i16,
            more: *mut u8,
        ) -> i32;
    }

    pub struct Mpu6500Drv {
        i2c:Option<I2c>,
        logger: DltLogger
    }

    static mut I2C_INSTANCE: Option<I2c> = None;
    static mut DLT_LOGGER_INSTANCE: Option<DltLogger> = None;

    #[no_mangle]
    pub extern "C" fn i2c_write(addr: u8, reg: u8, len: u8, data: *mut u8) -> i32 {
        unsafe {
            let logger = DLT_LOGGER_INSTANCE.as_mut().unwrap();
            match I2C_INSTANCE {
                Some(ref mut i2c) => {
                    let slice = slice::from_raw_parts_mut(data, len as usize);
                    let mut buffer = [0u8; 256];

                    buffer[0] = reg;
                    buffer[1..=len as usize].copy_from_slice(&slice);
                    let res = i2c.write(&buffer[0..len as usize + 1]);
                    match res {
                        Ok(_) => 0,
                        Err(_) => {
                            dlt_err!(logger, "i2c_write failed");
                            -1
                        }
                    }
                }
                None => {
                    dlt_err!(logger, "I2C is not initialized");
                    -1
                }
            }
        }
    }

    #[no_mangle]
    pub extern "C" fn i2c_read(addr: u8, reg: u8, len: u8, data: *mut u8) -> i32 {
        unsafe {
            let logger = DLT_LOGGER_INSTANCE.as_mut().unwrap();
            match I2C_INSTANCE {
                Some(ref mut i2c) => {
                    let mut slice = slice::from_raw_parts_mut(data, len as usize);
                    // info!("i2c_read addr: {}, reg: {}, len: {}", addr, reg, len);
                    let res = i2c.write_read(&[reg], &mut slice);
                    // info!("i2c_read res: {:?}, data {}", res, slice);
                    match res {
                        Ok(_) => 0,
                        Err(_) => {
                            dlt_err!(logger, "i2c_read failed");
                            -1
                        }
                    }
                }
                None => {
                    dlt_err!(logger, "I2C is not initialized");
                    -1
                }
            }
        }
    }

    #[no_mangle]
    pub extern "C" fn rust_log_i(msg: *const u8) {
        let c_str = unsafe {
            assert!(!msg.is_null());
            CStr::from_ptr(msg)
        };
        unsafe {
            let logger = DLT_LOGGER_INSTANCE.as_mut().unwrap();
            dlt_info!(logger, "{}", c_str.to_str().unwrap());
        }
    }

    #[no_mangle]
    pub extern "C" fn rust_log_e(msg: *const u8) {
        let c_str = unsafe {
            assert!(!msg.is_null());
            CStr::from_ptr(msg)
        };
        unsafe {
            let logger = DLT_LOGGER_INSTANCE.as_mut().unwrap();
            dlt_err!(logger, "{}", c_str.to_str().unwrap());
        }
    }

    #[no_mangle]
    pub extern "C" fn delay_ms(ms: u32) {
        std::thread::sleep(std::time::Duration::from_millis(ms as u64));
    }

    #[no_mangle]
    pub extern "C" fn get_time_ms() -> u32 {
        let now = SystemTime::now();
        now.duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_millis() as u32
    }

    impl Mpu6500Drv {
        pub fn new(mut i2c_bus: I2c) -> Self {
            let mut logger = DltLogger::new("SHAL", "MpuLib");
            i2c_bus.set_slave_address(0x68).unwrap_or_else(|e| {
                dlt_err!(logger, "set_slave_address failed {}", e);
                }   
            );
            unsafe{
                DLT_LOGGER_INSTANCE = Some(DltLogger::new("SHAL", "MpuLib"));
            }
            Mpu6500Drv { i2c: Some(i2c_bus), logger: logger }
        }


        fn inv_row_2_scale(&mut self, row: &[i8]) -> u16 {
            let b: u16;
            if row[0] > 0 {
                b = 0;
            } else if row[0] < 0 {
                b = 4;
            } else if row[1] > 0 {
                b = 1;
            } else if row[1] < 0 {
                b = 5;
            } else if row[2] > 0 {
                b = 2;
            } else if row[2] < 0 {
                b = 6;
            } else {
                b = 7; // error
                dlt_err!(self.logger, "invalid row")
            }
            return b;
        }

        fn inv_orientation_matrix_to_scalar(&mut self, mtx: &[i8; 9]) -> u16 {
            let mut scalar = self.inv_row_2_scale(&mtx[0..3]);
            scalar |= self.inv_row_2_scale(&mtx[3..6]) << 3;
            scalar |= self.inv_row_2_scale(&mtx[6..9]) << 6;

            scalar
        }

        pub fn init(&mut self) {
            unsafe {
                I2C_INSTANCE = Some(self.i2c.take().unwrap());
                let mut gyro_rate: u16 = 0;
                let mut gyro_fsr: u16 = 0;
                let mut accel_fsr: u16 = 0;
                dlt_info!(self.logger, "mpu6500 init");
                let result = mpu_init();
                if result != 0 {
                    dlt_err!(self.logger, "mpu_init failed");
                }
                dlt_info!(self.logger, "mpu_set_sensors");
                let result = mpu_set_sensors(INV_XYZ_GYRO | INV_XYZ_ACCEL);
                if result != 0 {
                    dlt_err!(self.logger, "mpu_set_sensors failed");
                }
                dlt_info!(self.logger, "mpu_configure_fifo");
                let result = mpu_configure_fifo(INV_XYZ_GYRO | INV_XYZ_ACCEL);
                if result != 0 {
                    dlt_err!(self.logger, "mpu_configure_fifo failed");
                }
                dlt_info!(self.logger, "mpu_set_sample_rate");
                let result = mpu_set_sample_rate(MPU_200_HZ);
                if result != 0 {
                    dlt_err!(self.logger, "mpu_set_sample_rate failed");
                }
                dlt_info!(self.logger, "mpu_get_sample_rate");
                let result = mpu_get_sample_rate(&mut gyro_rate);
                if result != 0 {
                    dlt_err!(self.logger, "mpu_get_sample_rate failed");
                }
                dlt_info!(self.logger, "mpu_get_gyro_fsr");
                let result = mpu_get_gyro_fsr(&mut gyro_fsr);
                if result != 0 {
                    dlt_err!(self.logger, "mpu_get_gyro_fsr failed");
                }
                dlt_info!(self.logger, "mpu_get_accel_fsr");
                let result = mpu_get_accel_fsr(&mut accel_fsr);
                if result != 0 {
                    dlt_err!(self.logger, "mpu_get_accel_fsr failed");
                }
                dlt_info!(self.logger, "Gyro rate set to {} Hz\n", gyro_rate);
                dlt_info!(self.logger, "Gyro fsr set to {} dps\n", gyro_fsr);
                dlt_info!(self.logger, "Accel fsr set to {} g\n", accel_fsr);
                dlt_info!(self.logger, "Initializing DMP");
                let result = dmp_load_motion_driver_firmware();
                if result != 0 {
                    dlt_err!(self.logger, "dmp_load_motion_driver_firmware failed {}", result);
                }
                dlt_info!(self.logger, "dmp_set_orientation");
                let result =
                    dmp_set_orientation(self.inv_orientation_matrix_to_scalar(&GYRO_ORIENTATION));
                if result != 0 {
                    dlt_err!(self.logger,"dmp_set_orientation failed");
                }
                dlt_info!(self.logger,"dmp_enable_feature");
                let result = dmp_enable_feature(
                    DMP_FEATURE_6X_LP_QUAT
                        | DMP_FEATURE_SEND_RAW_ACCEL
                        | DMP_FEATURE_SEND_CAL_GYRO
                        | DMP_FEATURE_GYRO_CAL,
                );
                if result != 0 {
                    dlt_err!(self.logger,"dmp_enable_feature failed");
                }
                dlt_info!(self.logger,"dmp_set_fifo_rate");
                let result = dmp_set_fifo_rate(MPU_200_HZ);
                if result != 0 {
                    dlt_err!(self.logger,"dmp_set_fifo_rate failed");
                }
                dlt_info!(self.logger,"mpu_set_dmp_state");
                let result = mpu_set_dmp_state(1);
                if result != 0 {
                    dlt_err!(self.logger,"mpu_set_dmp_state failed");
                }
                self.i2c = Some(I2C_INSTANCE.take().unwrap());
            }
        }

        pub fn read_sensor_data(
            &mut self,
            sens_data: &mut SensorData,
        ) -> (i32, u8) {
            let result: i32;
            let mut all_data_avaibl: i16 =
                INV_XYZ_ACCEL as i16 | INV_XYZ_GYRO as i16 | INV_WXYZ_QUAT;
            let mut more: u8 = 0;
            unsafe {
                I2C_INSTANCE = Some(self.i2c.take().unwrap());
                result = dmp_read_fifo(
                    &mut sens_data.gyro[0],
                    &mut sens_data.accel[0],
                    &mut sens_data.quat[0],
                    &mut sens_data.timestamp,
                    &mut all_data_avaibl,
                    &mut more,
                );
                self.i2c = Some(I2C_INSTANCE.take().unwrap());
                (result, more)
            }
        }
    }
}
