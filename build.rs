//! This build script copies the `memory.x` file from the crate root into
//! a directory where the linker can always find it at build time.
//! For many projects this is optional, as the linker always searches the
//! project root directory -- wherever `Cargo.toml` is. However, if you
//! are using a workspace or have a more complicated build setup, this
//! build script becomes required. Additionally, by requesting that
//! Cargo re-run the build script whenever `memory.x` is changed,
//! updating `memory.x` ensures a rebuild of the application with the
//! new memory settings.

use cc;
fn main() {

    cc::Build::new()
        .include("mpu6500_lib/core")
        .include("mpu6500_lib/mini-printf")
        .file("mpu6500_lib/core/inv_mpu_dmp_motion_driver.c")
        .file("mpu6500_lib/core/inv_mpu.c")
        .file("mpu6500_lib/mini-printf/mini-printf.c")
        .compile("sens_lib");
}
